package exercicisFuncions

import java.util.*

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/12/14
* TITLE: Eleva’l
*/

fun main(){
    val scanner = Scanner(System.`in`)

    val entrada1 = scanner.nextLong()
    val entrada2 = scanner.nextLong()
    println(elevador(entrada1, entrada2))
}

fun elevador(num:Long, elevador:Long):Long{
    var resultado:Long = 1
    for(i in 1..elevador){
        resultado *= num
    }
    return resultado
}