import java.util.Scanner

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/12/07
* TITLE: Mida d’un String
*/

fun main(){
    val scanner = Scanner(System.`in`)
    val entrada = scanner.nextLine()
    println(fakeLength(entrada))

}
fun fakeLength(entrada: String):Int {
    return entrada.lastIndex+1
}