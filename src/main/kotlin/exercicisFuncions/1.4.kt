import java.util.Scanner

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/12/07
* TITLE: Subseqüència
*/

fun main(){
    val scanner = Scanner(System.`in`)
    val entrada = scanner.nextLine()
    val posicion = scanner.nextInt()
    val posicion2 = scanner.nextInt()

    println(letrasPosiciones(entrada, posicion, posicion2))

}
fun letrasPosiciones(texto:String, posicion:Int, posicion2:Int):String{
    var respuesta = ""
    if(posicion2 <= texto.lastIndex && posicion>= 0) {
        for(i in posicion until posicion2){
            respuesta+= texto[i]
        }
        return respuesta
    }
    return "La subseqüència $posicion-$posicion2 de l'String no existeix"
}
