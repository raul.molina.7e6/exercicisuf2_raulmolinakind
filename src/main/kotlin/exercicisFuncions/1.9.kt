package exercicisFuncions

import java.util.Scanner

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/12/14
* TITLE: Escriu una creu
*/

fun main(){
    val scanner = Scanner(System.`in`)

    val tamaño = scanner.nextInt()
    val forma = scanner.next()
    hacedorDeCruces(tamaño, forma)
}
fun hacedorDeCruces(size: Int, form: String){
    for(i in 0 until(size/2)){
        for(j in 0 until(size/2)){
            print(" ")
        }
        print("$form\n")
    }
    for(i in 0 until size){
        print(form)
    }
    println()
    for(i in 0 until(size/2)){
        for(j in 0 until(size/2)){
            print(" ")
        }
        print("$form\n")
    }
}
