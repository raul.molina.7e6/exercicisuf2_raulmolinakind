package exercicisFuncions

import java.util.*

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/12/14
* TITLE: Escriu una creu
*/

fun main(){
    val scanner = Scanner(System.`in`)
    val texto = scanner.nextLine()
    val recuadro = scanner.next()
    encuadradora(texto, recuadro)

}

fun encuadradora(texto:String, forma:String){
    for(i in 0 .. texto.lastIndex+4){
        print(forma)
    }
    println()
    for(i in 0 .. texto.lastIndex+4){
        if(i == 0 || i == texto.lastIndex+4){
            print(forma)
        }else{
            print(" ")
        }
    }
    println()
    for(i in 0 .. texto.lastIndex+4){
        if(i == 0 || i == texto.lastIndex+4){
            print(forma)
        }else if(i==1 || i == texto.lastIndex+4-1){
            print(" ")
        }
        else{
            print(texto[i-2])
        }
    }
    println()
    for(i in 0 .. texto.lastIndex+4){
        if(i == 0 || i == texto.lastIndex+4){
            print(forma)
        }else{
            print(" ")
        }
    }
    println()
    for(i in 0 .. texto.lastIndex+4){
        print(forma)
    }
}