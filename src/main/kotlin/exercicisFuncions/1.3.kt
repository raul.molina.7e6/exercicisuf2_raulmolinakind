import java.util.Scanner

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/12/07
* TITLE: Caràcter d’un String
*/

fun main(){
    val scanner = Scanner(System.`in`)
    val entrada = scanner.nextLine()
    val posicion = scanner.nextInt()

    println(letraPosicion(entrada, posicion))

}
fun letraPosicion(texto:String, posicion:Int):String{
    if(posicion <= texto.lastIndex)return texto[posicion].toString()
    return "La mida de l'String és inferior a $posicion"
}