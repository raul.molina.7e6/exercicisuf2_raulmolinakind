import java.util.Scanner

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/12/07
* TITLE: D’String a MutableList<Char>
*/

fun main(){
    val scanner = Scanner(System.`in`)
    val entrada = scanner.nextLine()

    println(pasarAlista(entrada))
}
fun pasarAlista(entrada: String):MutableList<Char>{
    val lista = mutableListOf<Char>()
    for(i in entrada){
        lista.add(i)
    }
    return lista
}