package exercicisFuncions

import java.util.Scanner

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/12/14
* TITLE: Escriu una línia
*/

fun main(){
    val scanner = Scanner(System.`in`)

    val espacios = scanner.nextInt()
    val numCaracters = scanner.nextInt()
    val queCaracters = scanner.next()
    println(escriuUnaLinea(espacios, numCaracters, queCaracters))
}

fun escriuUnaLinea(espacios:Int, numCaracters:Int, queCaracter:String): String{
    var respuesta = ""
    if(espacios > 0){
        for(i in 1..espacios){
            respuesta += " "
        }
    }
    for(i in 0..numCaracters-1){
        respuesta += queCaracter
    }
    return respuesta
}