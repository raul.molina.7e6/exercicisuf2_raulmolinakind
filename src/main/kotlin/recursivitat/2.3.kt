package recursivitat

import java.util.Scanner

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/12/14
* TITLE: Factorial
*/

fun main(){
    val scanner = Scanner(System.`in`)
    val entrada = scanner.nextInt()
    println(contadorDigitos(entrada))
}

fun contadorDigitos(entrada: Int):Int{
    if(entrada > 0)return 1 + contadorDigitos(entrada/10)
    else return 0
}