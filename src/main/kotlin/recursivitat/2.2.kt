package recursivitat

import java.util.Scanner

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/12/14
* TITLE: Factorial
*/

fun main(){
    val scanner = Scanner(System.`in`)
    val entrada = scanner.nextLong()
    println(factorialDoble(entrada))
}

fun factorialDoble(entrada: Long):Long {
    if (entrada > 1) return entrada * factorialDoble(entrada-2)
    else return 1
}