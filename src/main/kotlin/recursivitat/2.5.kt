package recursivitat

import java.util.*

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/12/14
* TITLE: Reducció de dígits
*/

fun main(){
    val scanner = Scanner(System.`in`)
    val entrada = scanner.nextInt()
    println(reduccionDeDigitos(entrada))
}

fun reduccionDeDigitos(entrada:Int): Int{
    if(contadorDigitos(entrada) != 1){
        return reduccionDeDigitos(sumaDeDigitos(entrada))
    }else return sumaDeDigitos(entrada)
}

fun sumaDeDigitos(entrada: Int):Int{
    var help = entrada
    var salida = 0
    for(i in 1.. contadorDigitos(entrada)){
        salida += help%10
        help/=10
    }
    return salida
}