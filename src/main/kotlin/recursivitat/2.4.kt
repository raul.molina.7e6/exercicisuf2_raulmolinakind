package recursivitat

import java.util.*

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/12/14
* TITLE: Reducció de dígits
*/

fun main(){
    val scanner = Scanner(System.`in`)
    val entrada = scanner.nextInt()
    println(nombresCreixents(entrada))
}

fun nombresCreixents(entrada: Int): Boolean{
    if(entrada<10)return true //Solo tiene un digito y es porque todo estaba en orden
    val numActual = entrada%10
    val numSiguiente = (entrada/10)%10
    if(numActual<=numSiguiente)return false //Mira si es mas pequeño o igual
    return nombresCreixents(entrada/10)
}