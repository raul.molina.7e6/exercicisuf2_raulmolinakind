package recursivitat

import java.util.Scanner

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/12/14
* TITLE: Factorial
*/

fun main(){
    val scanner = Scanner(System.`in`)
    val entrada = scanner.nextLong()
    println(factorial(entrada))
}

fun factorial(entrada: Long):Long {
    if (entrada > 1) return entrada * factorial(entrada-1)
    else return 1
}